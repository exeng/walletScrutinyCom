---
wsId: ZamWallet
title: ZamWallet Crypto, DeFi, Invest
altTitle: 
authors:
- danny
appId: zamzam.wallet.app
appCountry: ru
idd: 1436344249
released: 2018-10-17
updated: 2022-10-12
version: 3.2.0
stars: 4.3
reviews: 72
size: '43224064'
website: https://zam.io
repository: 
issue: 
icon: zamzam.wallet.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-04
signer: 
reviewArchive: 
twitter: zam_io
social:
- https://www.linkedin.com/company/11770701

---

{% include copyFromAndroid.html %}
