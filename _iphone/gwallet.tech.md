---
wsId: GWalletApp
title: GWallet App
altTitle: 
authors:
- danny
appId: gwallet.tech
appCountry: be
idd: 1552665993
released: 2021-02-17
updated: 2023-01-18
version: 1.2.1
stars: 0
reviews: 0
size: '32723968'
website: https://gwallet.tech/
repository: 
issue: 
icon: gwallet.tech.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-10
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
