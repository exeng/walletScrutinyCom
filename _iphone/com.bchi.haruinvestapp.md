---
wsId: HaruInvest
title: 'Haru Invest: Earn Crypto & BTC'
altTitle: 
authors:
- danny
appId: com.bchi.haruinvestapp
appCountry: us
idd: 1579344792
released: 2021-08-19
updated: 2022-12-20
version: 2.13.0
stars: 4.9
reviews: 174
size: '91963392'
website: https://haruinvest.com/
repository: 
issue: 
icon: com.bchi.haruinvestapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-11
signer: 
reviewArchive: 
twitter: haruinvest
social:
- https://www.facebook.com/haruinvest

---

{% include copyFromAndroid.html %}
