---
wsId: Koinstrap
title: 'Koinstrap: Buy BTC, ETH, SHIB'
altTitle: 
authors:
- danny
appId: com.koinstrap.koinstrap
appCountry: us
idd: 1527922541
released: 2020-08-31
updated: 2022-11-02
version: 3.1.5
stars: 4.5
reviews: 23
size: '18192384'
website: https://koinstrap.com/
repository: 
issue: 
icon: com.koinstrap.koinstrap.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-22
signer: 
reviewArchive: 
twitter: koinstrap
social:
- https://www.facebook.com/koinstrap

---

{% include copyFromAndroid.html %}
