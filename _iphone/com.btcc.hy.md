---
wsId: btcc
title: BTCC-Trade Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.btcc.hy
appCountry: us
idd: 1462880009
released: 2019-05-11
updated: 2023-01-19
version: 7.0.1
stars: 4
reviews: 68
size: '243101696'
website: https://www.btcc.com/en-US
repository: 
issue: 
icon: com.btcc.hy.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: YourBTCC
social:
- https://www.linkedin.com/company/yourbtcc
- https://www.facebook.com/yourbtcc
- https://www.reddit.com/r/YourBTCC

---

 {% include copyFromAndroid.html %}
