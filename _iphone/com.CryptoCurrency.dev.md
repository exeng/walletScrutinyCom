---
wsId: crypterApp
title: The Crypto App - Coin Tracker
altTitle: 
authors:
- danny
appId: com.CryptoCurrency.dev
appCountry: us
idd: 1339112917
released: 2018-02-21
updated: 2022-11-17
version: 3.1.0
stars: 4.7
reviews: 2226
size: '184670208'
website: https://thecrypto.app
repository: 
issue: 
icon: com.CryptoCurrency.dev.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive: 
twitter: TrustSwap
social:
- https://www.linkedin.com/company/TrustSwap
- https://www.facebook.com/TrustSwap

---

{% include copyFromAndroid.html %}
