---
wsId: Bitmama
title: Bitmama
altTitle: 
authors:
- danny
appId: com.bitmama.bitmama.ios
appCountry: us
idd: 1561857024
released: 2021-06-30
updated: 2023-01-12
version: 1.0.64
stars: 3.7
reviews: 15
size: '102906880'
website: https://www.bitmama.io/
repository: 
issue: 
icon: com.bitmama.bitmama.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-10
signer: 
reviewArchive: 
twitter: bitmama
social:
- https://www.facebook.com/bitmama
- https://www.instagram.com/bitmamaexchange/

---

{% include copyFromAndroid.html %}
