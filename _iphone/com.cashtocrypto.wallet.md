---
wsId: bitcoindepot
title: Bitcoin Depot
altTitle: 
authors:
- danny
appId: com.cashtocrypto.wallet
appCountry: us
idd: 1554808338
released: 2021-03-30
updated: 2022-12-13
version: 2.2.1
stars: 3.6
reviews: 73
size: '77744128'
website: https://bitcoindepot.com/
repository: 
issue: 
icon: com.cashtocrypto.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-08
signer: 
reviewArchive: 
twitter: bitcoin_depot
social:
- https://www.facebook.com/BitcoinDepot

---

{% include copyFromAndroid.html %}
