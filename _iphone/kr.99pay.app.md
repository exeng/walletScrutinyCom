---
wsId: 99Recharge
title: 99pay Recharge mobile & 00301
altTitle: 
authors:
- leo
appId: kr.99pay.app
appCountry: kr
idd: 1229582503
released: 2017-05-05
updated: 2022-09-28
version: 2.1.3
stars: 4.3
reviews: 24
size: '60221440'
website: http://www.99pay.kr
repository: 
issue: 
icon: kr.99pay.app.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-12-26
signer: 
reviewArchive: 
twitter: 
social: 

---

You probably are looking for the other 99pay app:

{% include walletLink.html wallet='iphone/com.pay99' verdict='true' %}