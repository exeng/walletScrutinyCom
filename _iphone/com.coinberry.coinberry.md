---
wsId: CoinBerry
title: 'Coinberry: Buy and sell crypto'
altTitle: 
authors:
- danny
appId: com.coinberry.coinberry
appCountry: ca
idd: 1370601820
released: 2018-06-09
updated: 2022-11-24
version: '136.00'
stars: 4.5
reviews: 5934
size: '55636992'
website: https://coinberry.com
repository: 
issue: 
icon: com.coinberry.coinberry.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: CoinberryHQ
social:
- https://www.linkedin.com/company/coinberry
- https://www.facebook.com/CoinberryOfficial

---

{% include copyFromAndroid.html %}
