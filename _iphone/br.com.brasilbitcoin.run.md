---
wsId: brasilBitcoin
title: 'Brasil Bitcoin: Cripto Grátis'
altTitle: 
authors:
- danny
appId: br.com.brasilbitcoin.run
appCountry: br
idd: 1519300849
released: 2020-07-27
updated: 2023-01-07
version: 2.9.09
stars: 4.6
reviews: 1572
size: '93822976'
website: 
repository: 
issue: 
icon: br.com.brasilbitcoin.run.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive: 
twitter: brbtcoficial
social:
- https://www.facebook.com/brbtcoficial

---

{% include copyFromAndroid.html %}
