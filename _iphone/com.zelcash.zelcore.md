---
wsId: ZelCore
title: ZelCore
altTitle: 
authors:
- leo
appId: com.zelcash.zelcore
appCountry: 
idd: 1436296839
released: 2018-09-23
updated: 2023-01-09
version: v6.3.0
stars: 3.6
reviews: 131
size: '83186688'
website: https://zelcore.io
repository: 
issue: 
icon: com.zelcash.zelcore.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: zelcash
social:
- https://www.reddit.com/r/ZelCash

---

{% include copyFromAndroid.html %}
