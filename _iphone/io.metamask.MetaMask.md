---
wsId: metamask
title: MetaMask - Blockchain Wallet
altTitle: 
authors:
- leo
appId: io.metamask.MetaMask
appCountry: 
idd: 1438144202
released: 2020-09-03
updated: 2023-01-18
version: 5.13.0
stars: 4.7
reviews: 33270
size: '63378432'
website: https://metamask.io/
repository: 
issue: 
icon: io.metamask.MetaMask.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-05-01
signer: 
reviewArchive: 
twitter: 
social: 

---

This is an ETH-only app and thus not a Bitcoin wallet.
