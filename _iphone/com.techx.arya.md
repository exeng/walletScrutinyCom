---
wsId: aryainvest
title: 'ARYA : Investor Community'
altTitle: 
authors:
- danny
appId: com.techx.arya
appCountry: us
idd: 1478620685
released: 2019-09-06
updated: 2023-01-05
version: 2.20.8
stars: 4.8
reviews: 11
size: '119845888'
website: https://aryatrading.com/#pricing
repository: 
issue: 
icon: com.techx.arya.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-09-11
signer: 
reviewArchive: 
twitter: TheAryaApp
social:
- https://www.linkedin.com/company/thearyaapp
- https://www.facebook.com/WeloveArya

---

{% include copyFromAndroid.html %}
