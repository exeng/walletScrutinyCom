---
wsId: Ibandirect
title: Ibandirect
altTitle: 
authors:
- danny
appId: com.ibandirect.cards
appCountry: sg
idd: 1538001175
released: 2020-11-09
updated: 2023-01-04
version: 1.7.5
stars: 0
reviews: 0
size: '125984768'
website: https://ibandirect.com/
repository: 
issue: 
icon: com.ibandirect.cards.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-10
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
