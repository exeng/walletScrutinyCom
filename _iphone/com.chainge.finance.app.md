---
wsId: ChaingeFinance
title: Chainge Finance
altTitle: 
authors:
- danny
appId: com.chainge.finance.app
appCountry: us
idd: 1578987516
released: 2021-08-04
updated: 2022-12-15
version: 0.5.23
stars: 4.6
reviews: 38
size: '89468928'
website: https://www.chainge.finance/
repository: 
issue: 
icon: com.chainge.finance.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-21
signer: 
reviewArchive: 
twitter: FinanceChainge
social:
- https://www.linkedin.com/company/chainge-finance
- https://www.facebook.com/chainge.finance

---

{% include copyFromAndroid.html %}
