---
wsId: CoinSwitch
title: CoinSwitch
altTitle: 
authors:
- danny
appId: com.coinswitch.kuber
appCountry: in
idd: 1540214951
released: 2020-12-01
updated: 2023-01-15
version: 4.4.1
stars: 4.5
reviews: 45807
size: '105396224'
website: https://coinswitch.co/in
repository: 
issue: 
icon: com.coinswitch.kuber.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-21
signer: 
reviewArchive: 
twitter: CoinSwitchKuber
social:
- https://www.linkedin.com/company/coinswitch
- https://www.facebook.com/coinswitch

---

{% include copyFromAndroid.html %}
