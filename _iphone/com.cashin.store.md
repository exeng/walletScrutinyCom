---
wsId: BitcoinPoint
title: BitcoinPoint
altTitle: 
authors:
- danny
appId: com.cashin.store
appCountry: gb
idd: 1363753409
released: 2018-08-15
updated: 2023-01-19
version: '5.9'
stars: 4.3
reviews: 6
size: '106401792'
website: https://bitcoinpoint.com
repository: 
issue: 
icon: com.cashin.store.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-17
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
