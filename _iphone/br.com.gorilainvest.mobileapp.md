---
wsId: gorilaInvest
title: 'Gorila: gestão de investimento'
altTitle: 
authors:
- danny
appId: br.com.gorilainvest.mobileapp
appCountry: us
idd: '1447950043'
released: '2019-01-27T14:35:53Z'
updated: 2023-01-07
version: 3.0.2
stars: 4.7
reviews: 198
size: '41924608'
website: https://gorila.com.br/
repository: 
issue: 
icon: br.com.gorilainvest.mobileapp.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2022-06-24
signer: 
reviewArchive: 
twitter: gorilainvest
social:
- https://www.facebook.com/GorilaInvest
- https://www.instagram.com/gorilainvest
- https://www.youtube.com/channel/UCYJiIU0DqDLiPcq8tWecjdw
- https://www.linkedin.com/company/gorila

---

{% include copyFromAndroid.html %}
