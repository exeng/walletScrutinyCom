---
wsId: LBank
title: LBank - Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.LBank.LBankNavApp
appCountry: us
idd: 1437346368
released: 2019-02-22
updated: 2023-01-16
version: 4.9.49
stars: 3.9
reviews: 478
size: '218755072'
website: https://www.lbank.info/
repository: 
issue: 
icon: com.LBank.LBankNavApp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: LBank_Exchange
social:
- https://www.linkedin.com/company/lbank
- https://www.facebook.com/LBank.info

---

{% include copyFromAndroid.html %}
