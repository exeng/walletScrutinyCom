---
wsId: bitcointrade
title: BitcoinTrade - Criptomoedas
altTitle: 
authors:
- danny
appId: com.root.BitcoinTrade
appCountry: br
idd: 1320032339
released: 2017-12-13
updated: 2023-01-05
version: 4.4.4
stars: 3.9
reviews: 910
size: '58525696'
website: http://www.bitcointrade.com.br/
repository: 
issue: 
icon: com.root.BitcoinTrade.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive: 
twitter: 
social:
- https://www.linkedin.com/company/bitcointrade
- https://www.facebook.com/BitcointradeBR

---

{% include copyFromAndroid.html %}
