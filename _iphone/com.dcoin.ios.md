---
wsId: dcoin
title: Dcoin - Bitcoin Exchange
altTitle: 
authors:
- danny
appId: com.dcoin.ios
appCountry: us
idd: 1508064925
released: 2018-12-20
updated: 2023-01-19
version: 5.6.4
stars: 3
reviews: 20
size: '62753792'
website: https://www.dcoin.com/
repository: 
issue: 
icon: com.dcoin.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive: 
twitter: dcoinexchange
social:
- https://www.linkedin.com/company/dcoin-exchange

---

 {% include copyFromAndroid.html %}
