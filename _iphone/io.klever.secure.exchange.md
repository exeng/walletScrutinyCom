---
wsId: kleverexchange
title: Klever Exchange
altTitle: 
authors:
- danny
appId: io.klever.secure.exchange
appCountry: us
idd: 1553486059
released: 2021-09-25
updated: 2023-01-07
version: 1.5.5
stars: 4.7
reviews: 53
size: '121762816'
website: https://klever.io
repository: 
issue: 
icon: io.klever.secure.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-05
signer: 
reviewArchive: 
twitter: klever_io
social:
- https://www.linkedin.com/company/klever-app
- https://www.facebook.com/klever.io

---

{% include copyFromAndroid.html %}
