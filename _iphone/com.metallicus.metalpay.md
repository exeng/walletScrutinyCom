---
wsId: MetalPay
title: 'Metal Pay: Buy & Sell Crypto'
altTitle: 
authors:
- danny
appId: com.metallicus.metalpay
appCountry: us
idd: 1345101178
released: 2018-09-14
updated: 2022-12-27
version: 2.9.19
stars: 4.3
reviews: 4213
size: '124825600'
website: https://metalpay.com
repository: 
issue: 
icon: com.metallicus.metalpay.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: metalpaysme
social:
- https://www.facebook.com/metalpaysme
- https://www.reddit.com/r/MetalPay

---

{% include copyFromAndroid.html %}

