---
wsId: WealthsimpleTrade
title: Wealthsimple
altTitle: 
authors:
- danny
appId: com.wealthsimple.trade
appCountry: ca
idd: 1403491709
released: 2019-02-26
updated: 2023-01-16
version: 2.73.0
stars: 4.7
reviews: 123626
size: '176367616'
website: https://www.wealthsimple.com/en-ca/
repository: 
issue: 
icon: com.wealthsimple.trade.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-09-03
signer: 
reviewArchive: 
twitter: Wealthsimple
social:
- https://www.facebook.com/wealthsimple

---

{% include copyFromAndroid.html %}
