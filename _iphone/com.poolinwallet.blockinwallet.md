---
wsId: PoolinWallet
title: 'Poolin Wallet: Bitcoin'
altTitle: 
authors:
- danny
appId: com.poolinwallet.blockinwallet
appCountry: us
idd: 1495275337
released: 2020-01-23
updated: 2022-12-30
version: 2.5.5
stars: 3.6
reviews: 37
size: '99765248'
website: https://poolin.fi/
repository: 
issue: 
icon: com.poolinwallet.blockinwallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive: 
twitter: PoolinWallet
social:
- https://www.linkedin.com/company/poolinwallet

---

{% include copyFromAndroid.html %}
