---
wsId: SALT
title: SALT Crypto Loans
altTitle: 
authors:
- danny
appId: com.saltlending.mobile
appCountry: us
idd: 1383851676
released: 2019-01-07
updated: 2022-12-20
version: 2.6.37
stars: 4.8
reviews: 24
size: '53827584'
website: https://saltlending.com/
repository: 
issue: 
icon: com.saltlending.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive: 
twitter: SALTlending
social:
- https://www.linkedin.com/company/saltlending
- https://www.facebook.com/SALTLENDING

---

{% include copyFromAndroid.html %}
