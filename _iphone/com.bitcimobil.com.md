---
wsId: bitci
title: Bitci, Bitcoin Altcoin Borsası
altTitle: 
authors:
- danny
appId: com.bitcimobil.com
appCountry: tr
idd: '1459044769'
released: '2019-04-17T02:30:04Z'
updated: 2021-09-07
version: 3.8.0
stars: 4.3
reviews: 1752
size: '52859904'
website: https://www.bitci.com/
repository: 
issue: 
icon: com.bitcimobil.com.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2022-12-26
signer: 
reviewArchive: 
twitter: bitcicom
social:
- https://www.linkedin.com/company/bitcicom/
- https://www.facebook.com/bitcicom
- https://medium.com/@bitcicom
- https://www.youtube.com/channel/UCJ_cGIv6JJ249qKXWbhOtMg

---

{% include copyFromAndroid.html %}