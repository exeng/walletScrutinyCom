---
wsId: cobowallet
title: 'Cobo Crypto Wallet: BTC & DASH'
altTitle: 
authors:
- leo
appId: cobo.wallet
appCountry: 
idd: 1406282615
released: 2018-08-05
updated: 2022-11-23
version: 5.19.6
stars: 3.7
reviews: 7
size: '81119232'
website: https://cobo.com
repository: 
issue: 
icon: cobo.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: Cobo_Wallet
social:
- https://www.linkedin.com/company/coboofficial
- https://www.facebook.com/coboOfficial

---

 {% include copyFromAndroid.html %}
