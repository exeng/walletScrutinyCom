---
wsId: ExodusCryptoBitcoinWallet
title: 'Exodus: Crypto Bitcoin Wallet'
altTitle: 
authors:
- leo
appId: exodus-movement.exodus
appCountry: 
idd: 1414384820
released: 2019-03-23
updated: 2023-01-06
version: 23.1.2
stars: 4.6
reviews: 18140
size: '78839808'
website: https://exodus.com/mobile
repository: 
issue: 
icon: exodus-movement.exodus.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-01-23
signer: 
reviewArchive: 
twitter: exodus_io
social:
- https://www.facebook.com/exodus.io

---

Just like {% include walletLink.html wallet='android/exodusmovement.exodus' %} on Android, this app is
closed source and thus **not verifiable**.
