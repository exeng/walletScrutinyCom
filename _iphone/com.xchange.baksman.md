---
wsId: Baksman
title: Baksman – buy Bitcoin
altTitle: 
authors:
- danny
appId: com.xchange.baksman
appCountry: ru
idd: 1436169013
released: 2018-09-21
updated: 2023-01-07
version: 2.2.5
stars: 4.6
reviews: 64
size: '14470144'
website: 
repository: 
issue: 
icon: com.xchange.baksman.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-15
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/baksmancom

---

{% include copyFromAndroid.html %}
