---
wsId: spatium
title: Spatium Keyless Bitcoin Wallet
altTitle: 
authors:
- danny
appId: capital.spatium.wallet
appCountry: us
idd: 1404844195
released: 2018-08-06
updated: 2022-11-30
version: 3.1.8
stars: 4.6
reviews: 32
size: '70413312'
website: https://spatium.net/
repository: 
issue: 
icon: capital.spatium.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-17
signer: 
reviewArchive: 
twitter: spatium_news
social:
- https://www.linkedin.com/company/spatium-capital
- https://www.facebook.com/spatiumnews

---

{% include copyFromAndroid.html %}
