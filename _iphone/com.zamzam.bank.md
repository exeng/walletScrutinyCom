---
wsId: ZamZam
title: Zamzam денежные переводы в СНГ
altTitle: 
authors:
- danny
appId: com.zamzam.bank
appCountry: ru
idd: 1521900439
released: 2020-07-04
updated: 2022-12-21
version: 1.9.1
stars: 3.8
reviews: 68
size: '81583104'
website: https://zam.me
repository: 
issue: 
icon: com.zamzam.bank.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-30
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}