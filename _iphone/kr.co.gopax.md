---
wsId: gopax
title: 고팍스 - GOPAX
altTitle: 
authors:
- danny
appId: kr.co.gopax
appCountry: kr
idd: 1369896843
released: 2018-06-21
updated: 2023-01-18
version: 2.1.9
stars: 2.9
reviews: 486
size: '109105152'
website: https://www.gopax.co.kr/notice
repository: 
issue: 
icon: kr.co.gopax.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: 
social: 

---

 {% include copyFromAndroid.html %}
