---
wsId: NamiExchange
title: 'Nami Exchange: Buy BTC, Crypto'
altTitle: 
authors:
- danny
appId: com.namicorp.exchange
appCountry: us
idd: '1480302334'
released: '2019-09-30T07:00:00Z'
updated: 2023-01-14
version: 2.0.8
stars: 2.9
reviews: 15
size: '88696832'
website: https://nami.io/
repository: 
issue: 
icon: com.namicorp.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-07-20
signer: 
reviewArchive: 
twitter: NamiTrade
social:
- https://www.reddit.com/r/NAMIcoin/
- https://www.facebook.com/groups/800880653641607/
- https://medium.com/nami-io

---

{% include copyFromAndroid.html %}