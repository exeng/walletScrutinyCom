---
wsId: ECOS
title: 'ECOS: Bitcoin & Crypto Mining'
altTitle: 
authors:
- danny
appId: am.ecos.ios.production
appCountry: us
idd: 1528964374
released: 2020-11-25
updated: 2022-12-30
version: 1.30.5
stars: 3.6
reviews: 131
size: '84228096'
website: https://ecos.am/
repository: 
issue: 
icon: am.ecos.ios.production.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive: 
twitter: ecosmining
social:
- https://www.facebook.com/ecosdefi

---

{% include copyFromAndroid.html %}
