---
wsId: AnchorUSD
title: Anchor - Send and Invest
altTitle: 
authors:
- danny
appId: app.anchors.anchorusd
appCountry: us
idd: 1495986023
released: 2020-01-30
updated: 2023-01-03
version: 1.18.10
stars: 4.3
reviews: 5183
size: '51863552'
website: https://www.tryanchor.com/
repository: 
issue: 
icon: app.anchors.anchorusd.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: anchorusd
social: 

---

{% include copyFromAndroid.html %}
