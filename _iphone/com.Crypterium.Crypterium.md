---
wsId: crypterium
title: Choise.com Buy & earn crypto
altTitle: 
authors:
- leo
appId: com.Crypterium.Crypterium
appCountry: 
idd: 1360632912
released: 2018-03-26
updated: 2022-12-26
version: '3.0'
stars: 4.2
reviews: 941
size: '454104064'
website: https://cards.crypterium.com/visa
repository: 
issue: 
icon: com.Crypterium.Crypterium.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-20
signer: 
reviewArchive: 
twitter: crypterium
social:
- https://www.facebook.com/crypterium.org

---

Judging by what we can find on the [wallet site](https://wallet.crypterium.com/):

> **Store**<br>
  keep your currencies<br>
  safe & fully insured

this is a custodial app as a self-custody wallet cannot ever have funds insured.

As a custodial app it is **not verifiable**.
