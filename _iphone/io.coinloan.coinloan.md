---
wsId: CoinLoan
title: CoinLoan Crypto Wallet & Loans
altTitle: 
authors:
- danny
appId: io.coinloan.coinloan
appCountry: us
idd: 1506572788
released: 2020-04-24
updated: 2022-12-22
version: 1.7.4
stars: 4.8
reviews: 419
size: '52673536'
website: https://coinloan.io
repository: 
issue: 
icon: io.coinloan.coinloan.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: coin_loan
social:
- https://www.linkedin.com/company/coinloan
- https://www.facebook.com/coinloan.project
- https://www.reddit.com/r/coinloan

---

{% include copyFromAndroid.html %}
