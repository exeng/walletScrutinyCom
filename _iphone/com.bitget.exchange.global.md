---
wsId: Bitget
title: Bitget- Buy Bitcoin & Ether
altTitle: 
authors:
- danny
appId: com.bitget.exchange.global
appCountry: ua
idd: 1442778704
released: 2018-11-29
updated: 2022-11-28
version: 1.2.68
stars: 2.8
reviews: 109
size: '245739520'
website: https://www.bitget.com/en
repository: 
issue: 
icon: com.bitget.exchange.global.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-17
signer: 
reviewArchive: 
twitter: bitgetglobal
social:
- https://www.linkedin.com/company/bitget
- https://www.facebook.com/BitgetGlobal

---

 {% include copyFromAndroid.html %}
