---
wsId: nexo
title: 'Nexo: Bitcoin & Crypto Wallet'
altTitle: 
authors:
- leo
appId: com.nexobank.wallet
appCountry: 
idd: 1455341917
released: 2019-06-30
updated: 2022-11-21
version: 2.2.43
stars: 3.8
reviews: 1068
size: '60554240'
website: https://nexo.io
repository: 
issue: 
icon: com.nexobank.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-22
signer: 
reviewArchive: 
twitter: NexoFinance
social:
- https://www.facebook.com/nexofinance
- https://www.reddit.com/r/Nexo

---

{% include copyFromAndroid.html %}
