---
wsId: forex4you
title: Forex4you - Online Trading
altTitle: 
authors:
- danny
appId: com.forex4you.ios
appCountry: th
idd: 1008039704
released: 2015-10-11
updated: 2022-12-26
version: 5.2.1
stars: 4.7
reviews: 306
size: '174484480'
website: https://www.forex4you.com/en/contacts/
repository: 
issue: 
icon: com.forex4you.ios.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-12-24
signer: 
reviewArchive:
- date: 2021-11-01
  version: '4.2'
  appHash: 
  gitRevision: a5f6ad88ff8926faf6f2ce111aff123860ea1e50
  verdict: nosendreceive
twitter: 
social: 

---

{% include copyFromAndroid.html %}
