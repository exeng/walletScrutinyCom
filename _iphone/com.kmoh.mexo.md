---
wsId: Mexo
title: 'TruBit Pro: Crypto Exchange'
altTitle: 
authors:
- danny
appId: com.kmoh.mexo
appCountry: us
idd: 1555609032
released: 2021-03-01
updated: 2022-11-19
version: 3.0.4
stars: 4.9
reviews: 211
size: '110501888'
website: https://help.trubit.com/en
repository: 
issue: 
icon: com.kmoh.mexo.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: mexo_io
social:
- https://www.linkedin.com/company/mexoio
- https://www.facebook.com/mexo.io

---

{% include copyFromAndroid.html %}
