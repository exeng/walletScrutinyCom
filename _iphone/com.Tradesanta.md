---
wsId: TradeSanta
title: 'TradeSanta: Crypto Trading Bot'
altTitle: 
authors:
- danny
appId: com.Tradesanta
appCountry: us
idd: 1457051269
released: 2019-05-18
updated: 2022-10-08
version: 2.5.90
stars: 3.8
reviews: 33
size: '57104384'
website: https://tradesanta.com/en
repository: 
issue: 
icon: com.Tradesanta.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-10-16
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}

