---
wsId: niceHash
title: NiceHash
altTitle: 
authors:
- danny
appId: com.nicehash.mobile
appCountry: us
idd: '1372054956'
released: '2020-04-29T07:00:00Z'
updated: 2023-01-16
version: 5.2.1
stars: 4.3
reviews: 1334
size: '93758464'
website: https://www.nicehash.com/
repository: 
issue: 
icon: com.nicehash.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-06-24
signer: 
reviewArchive: 
twitter: NiceHashMining
social:
- https://www.linkedin.com/company/nicehash
- https://www.instagram.com/nicehash_official
- https://www.reddit.com/r/NiceHash
- https://www.facebook.com/NiceHash

---

{% include copyFromAndroid.html %}
