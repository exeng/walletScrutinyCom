---
wsId: bittrex
title: Bittrex | Buy Bitcoin & Crypto
altTitle: 
authors:
- leo
appId: com.bittrex.trade
appCountry: 
idd: 1465314783
released: 2019-12-19
updated: 2023-01-10
version: 1.20.1
stars: 4.6
reviews: 3342
size: '94539776'
website: https://bittrex.com/mobile
repository: 
issue: 
icon: com.bittrex.trade.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-02-09
signer: 
reviewArchive: 
twitter: BittrexGlobal
social:
- https://www.facebook.com/BittrexGlobal

---

This app is an interface to a trading platform:

> The Bittrex Global mobile app allows you to take the premiere crypto trading
  platform with you wherever you go.

As such, it lets you access your account with them but not custody your own
coins and therefore is **not verifiable**.
