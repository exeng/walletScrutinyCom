---
wsId: Defiant
title: Defiant Wallet
altTitle: 
authors:
- danny
appId: com.andinadefi.defiant
appCountry: ar
idd: 1559622756
released: 2021-04-07
updated: 2022-12-30
version: 4.1.4+251
stars: 4.9
reviews: 19
size: '83365888'
website: https://defiantapp.tech/
repository: 
issue: 
icon: com.andinadefi.defiant.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-15
signer: 
reviewArchive: 
twitter: defiantApp
social: 

---

{% include copyFromAndroid.html %}
