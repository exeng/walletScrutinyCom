---
wsId: tapngo
title: Tap - Buy & Sell Bitcoin
altTitle: 
authors:
- danny
appId: com.TapGlobal.tap
appCountry: gb
idd: 1492263993
released: 2019-12-20
updated: 2022-12-21
version: 2.6.4
stars: 4.7
reviews: 960
size: '193138688'
website: https://www.tap.global/
repository: 
issue: 
icon: com.TapGlobal.tap.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive: 
twitter: 
social: 

---

 {% include copyFromAndroid.html %}
