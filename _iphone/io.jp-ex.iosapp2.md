---
wsId: JPEX
title: JPEX Wallet
altTitle: 
authors:
- danny
appId: io.jp-ex.iosapp2
appCountry: jp
idd: 1559708728
released: 2021-04-22
updated: 2023-01-05
version: 2.66.720
stars: 0
reviews: 0
size: '85086208'
website: https://jp-ex.io/
repository: 
issue: 
icon: io.jp-ex.iosapp2.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}

