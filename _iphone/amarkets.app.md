---
wsId: AMarkets
title: AMarkets Trading
altTitle: 
authors:
- danny
appId: amarkets.app
appCountry: us
idd: 1495820700
released: 2020-02-12
updated: 2022-12-27
version: 1.4.65
stars: 4.9
reviews: 562
size: '146094080'
website: https://www.amarkets.com/
repository: 
issue: 
icon: amarkets.app.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-13
signer: 
reviewArchive: 
twitter: 
social:
- https://www.linkedin.com/company/amarkets
- https://www.facebook.com/AMarketsFirm

---

{% include copyFromAndroid.html %}
