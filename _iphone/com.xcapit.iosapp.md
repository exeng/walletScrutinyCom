---
wsId: xcapit
title: Xcapit
altTitle: 
authors:
- danny
appId: com.xcapit.iosapp
appCountry: ar
idd: '1545648148'
released: '2020-12-28T08:00:00Z'
updated: 2023-01-11
version: 3.11.1
stars: 4.7
reviews: 31
size: '61191168'
website: https://xcapit.com
repository: 
issue: 
icon: com.xcapit.iosapp.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-07-13
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}