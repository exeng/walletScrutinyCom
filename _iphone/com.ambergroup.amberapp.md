---
wsId: ambercrypto
title: 'WhaleFin: buy Crypto, BTC, ETH'
altTitle: 
authors:
- danny
appId: com.ambergroup.amberapp
appCountry: us
idd: 1515652068
released: 2020-09-21
updated: 2022-12-11
version: 2.11.2
stars: 4.8
reviews: 384
size: '279813120'
website: https://www.whalefin.com
repository: 
issue: 
icon: com.ambergroup.amberapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive: 
twitter: ambergroup_io
social:
- https://www.linkedin.com/company/amberbtc
- https://www.facebook.com/ambergroup.io

---

{% include copyFromAndroid.html %}
