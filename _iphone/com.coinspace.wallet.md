---
wsId: coinspace
title: Coin Wallet - Bitcoin & Crypto
altTitle: 
authors:
- leo
appId: com.coinspace.wallet
appCountry: 
idd: 980719434
released: 2015-12-14
updated: 2022-12-16
version: 5.9.3
stars: 4.4
reviews: 185
size: '49174528'
website: https://coin.space/
repository: https://github.com/CoinSpace/CoinSpace
issue: 
icon: com.coinspace.wallet.jpg
bugbounty: https://www.openbugbounty.org//bugbounty/CoinAppWallet/
meta: ok
verdict: ftbfs
date: 2020-12-20
signer: 
reviewArchive: 
twitter: coinappwallet
social:
- https://www.linkedin.com/company/coin-space
- https://www.facebook.com/coinappwallet

---

{% include copyFromAndroid.html %}
