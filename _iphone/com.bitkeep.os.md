---
wsId: bitkeep
title: 'BitKeep: DeFi Wallet'
altTitle: 
authors:
- leo
appId: com.bitkeep.os
appCountry: 
idd: 1395301115
released: 2018-09-26
updated: 2022-12-31
version: 7.3.0
stars: 4.2
reviews: 191
size: '107926528'
website: https://bitkeep.com
repository: 
issue: 
icon: com.bitkeep.os.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: BitKeepOS
social:
- https://www.facebook.com/bitkeep
- https://github.com/bitkeepcom

---

 {% include copyFromAndroid.html %}
