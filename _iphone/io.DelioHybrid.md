---
wsId: DelioLending
title: Delio
altTitle: 
authors:
- danny
appId: io.DelioHybrid
appCountry: kr
idd: 1498891184
released: 2020-02-26
updated: 2022-12-23
version: 1.4.1
stars: 3
reviews: 22
size: '63849472'
website: https://www.delio.foundation/
repository: 
issue: 
icon: io.DelioHybrid.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: happydelio
social:
- https://www.facebook.com/delio.io

---

{% include copyFromAndroid.html %}
