---
wsId: BinanceTR
title: 'Binance TR: BTC | SHIB | DOGE'
altTitle: 
authors:
- danny
appId: com.binanceCloudTR.binance
appCountry: tr
idd: 1548636153
released: 2021-02-18
updated: 2022-12-27
version: 1.16.0
stars: 4.5
reviews: 25094
size: '145260544'
website: https://www.trbinance.com/
repository: 
issue: 
icon: com.binanceCloudTR.binance.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: BinanceTR
social:
- https://www.facebook.com/TRBinanceTR

---

{% include copyFromAndroid.html %}
