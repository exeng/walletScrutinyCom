---
wsId: swissborg
title: SwissBorg - Invest in Crypto
altTitle: 
authors:
- danny
appId: com.swissborg.ios
appCountry: gb
idd: 1442483481
released: 2020-03-31
updated: 2023-01-17
version: 1.52.0
stars: 4.2
reviews: 991
size: '98038784'
website: https://swissborg.com
repository: 
issue: 
icon: com.swissborg.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive: 
twitter: swissborg
social:
- https://www.linkedin.com/company/swissborg
- https://www.facebook.com/swissborg

---

 {% include copyFromAndroid.html %}