---
wsId: Forexcom
title: 'FOREX.com: CFD & Forex Trading'
altTitle: 
authors:
- danny
appId: com.gaincapital.forex
appCountry: gb
idd: 1506581586
released: 2020-10-14
updated: 2022-12-16
version: 1.124.2786
stars: 3.7
reviews: 40
size: '98812928'
website: https://www.forex.com/en-uk/
repository: 
issue: 
icon: com.gaincapital.forex.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-16
signer: 
reviewArchive: 
twitter: forexcom
social:
- https://www.facebook.com/FOREXcom

---

{% include copyFromAndroid.html %}

