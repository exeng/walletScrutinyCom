---
wsId: roseon
title: Roseon
altTitle: 
authors:
- danny
appId: com.roseon.finance.production
appCountry: vn
idd: 1559440997
released: 2021-05-24
updated: 2022-11-30
version: 2.3.42
stars: 4.7
reviews: 39
size: '106401792'
website: https://roseon.finance/
repository: 
issue: 
icon: com.roseon.finance.production.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: RoseonFinance
social:
- https://www.facebook.com/Roseon.Finance

---

{% include copyFromAndroid.html %}
