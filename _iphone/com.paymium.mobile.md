---
wsId: Paymium
title: Paymium - Bitcoin Platform
altTitle: 
authors:
- danny
appId: com.paymium.mobile
appCountry: us
idd: 1055288395
released: 2016-01-18
updated: 2022-10-20
version: 3.3.7
stars: 4.5
reviews: 2
size: '115092480'
website: https://www.paymium.com/
repository: 
issue: 
icon: com.paymium.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-03
signer: 
reviewArchive: 
twitter: paymium
social:
- https://www.linkedin.com/company/paymium
- https://www.facebook.com/Paymium

---

{% include copyFromAndroid.html %}
