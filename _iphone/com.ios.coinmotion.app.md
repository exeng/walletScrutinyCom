---
wsId: Coinmotion
title: 'Coinmotion: Crypto Investing'
altTitle: 
authors:
- danny
appId: com.ios.coinmotion.app
appCountry: in
idd: 1518765595
released: 2020-11-19
updated: 2022-12-13
version: 1.8.3
stars: 0
reviews: 0
size: '38372352'
website: https://coinmotion.com/
repository: 
issue: 
icon: com.ios.coinmotion.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-19
signer: 
reviewArchive: 
twitter: Coinmotion
social:
- https://www.linkedin.com/company/coinmotion
- https://www.facebook.com/coinmotion

---

{% include copyFromAndroid.html %}
