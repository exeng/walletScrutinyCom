---
wsId: bitcointradingcapital
title: Bitcoin trading - Capital.com
altTitle: 
authors:
- danny
appId: com.kapital.trade.crypto
appCountry: cz
idd: 1487443266
released: 2019-11-26
updated: 2023-01-16
version: 1.55.0
stars: 4.8
reviews: 811
size: '94426112'
website: https://capital.com/
repository: 
issue: 
icon: com.kapital.trade.crypto.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-09-03
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
