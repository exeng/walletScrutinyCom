---
wsId: BitnovoWallet
title: Bitnovo - Buy Bitcoin
altTitle: 
authors:
- danny
appId: com.bitnovo.Wallet
appCountry: us
idd: 1553430666
released: 2021-10-27
updated: 2023-01-16
version: 1.8.2
stars: 2.2
reviews: 5
size: '83820544'
website: https://www.bitnovo.com
repository: 
issue: 
icon: com.bitnovo.Wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-30
signer: 
reviewArchive: 
twitter: bitnovo
social:
- https://www.linkedin.com/company/Bitnovo
- https://www.facebook.com/BitcoinBitnovo

---

{% include copyFromAndroid.html %}
