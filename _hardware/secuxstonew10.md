---
title: SecuX STONE W10
appId: secuxstonew10
authors:
- kiwilamb
- danny
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions:
- 59
- 89
- 13
weight: 49
provider: SecuX Technology Inc.
providerWebsite: https://secuxtech.com
website: https://secuxtech.com/secux-w10-hardware-wallet/
shop: https://shop.secuxtech.com/products/w10-hardware-wallet-for-computer/
country: TW
price: 69USD
repository: 
issue: 
icon: secuxstonew10.png
bugbounty: 
meta: ok
verdict: nosource
date: 2022-11-18
signer: 
reviewArchive: 
twitter: SecuXwallet
social:
- https://www.linkedin.com/company/secuxtech
- https://www.facebook.com/secuxtech

---

## Features

  - Military-grade Infineon Secure Element chip
  - Tamper-proof sealing labels
  - Support 1000+ coins, tokens and NFT (ERC-721 & ERC-1155)
  - Largest 2.8 inch color touchscreen
  - Supports multiple OS on computer
  - Micro-B USB connection
  - Compatible recoverability with BIP32, 39, 44 standards
  - Up to 500 accounts available

To set up this wallet users must connect to SecuX's [official Web Wallet](https://wallet.secuxtech.com/secuxess/) via USB or Bluetooth.

Users can send and receive transactions on the Web Wallet and then confirm the transactions on the hardware wallet device.

Unforunately, SecuX's source code is **not available for review.**