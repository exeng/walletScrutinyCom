---
title: ColdLar Smart
appId: coldlarsmart
authors:
- kiwilamb
- danny
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions: 
weight: 
provider: ColdLar Information Technology Co.
providerWebsite: https://www.coldlar.com/
website: https://www.coldlar.com/Smart
shop: 
country: CH
price: 
repository: 
issue: 
icon: coldlarsmart.png
bugbounty: 
meta: ok
verdict: defunct
date: 2022-11-18
signer: 
reviewArchive: 
twitter: Coldlar
social: 

---

## Updated Review 2022-11-18

The official social media account has not seen activity since 2020 and the original site "coldlar.com" is down. Trying to access this site resulted in a page:

> The website is currently inaccessible...
>
> [The website has not obtained an ICP filing](https://www.alibabacloud.com/help/en/icp-filing/latest/what-is-an-icp-filing) according to the law of the Ministry of Industry and Information Technology of the People's Republic of China
>
> Relevant law: Measures for the Archival Administration of Non-commercial Internet Information Services

We found two reviews of this product from 2020. Both [link to](https://boinnex.com/coldlar-review-2020-pro-3-and-the-new-smart/) or [reference](https://www.youtube.com/watch?v=ndPclvNVnhY) the original, now defunct site.

**Note:**  While searching for the ColdLar Smart product online, we came across a website that looked very similar to ColdLar's official site. However its domain name was **"www.hengshengjia.com"** and there is no indication if it is actually related to ColdLar. Notably, this specific product was not listed on their website.

We did not find any available shops or vendors selling this product. Because of this, along with the website and the social media's inactivity, we will mark ColdLar Smart as **defunct.**