---
wsId: HODLCryptoTracker
title: HODL Real-Time Crypto Tracker
altTitle: 
authors:
- danny
users: 100000
appId: com.flashratings.hodl
appCountry: 
released: Jul 24, 2018
updated: 2022-12-15
version: '8.42'
stars: 4.7
ratings: 
reviews: 443
size: 
website: http://www.hodlfinance.com
repository: 
issue: 
icon: com.flashratings.hodl.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2022-06-24
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 

---

## App Description

This app is a tool for tracking crypto prices:

> Track your cryptocurrency in this crypto portfolio tracker & watch real-time crypto live charts & prices from all global exchanges in this crypto tracker app. Filtered tweets & breaking headlines from all top news sources. Simple & intuitive cryptocurrency tracker interface & all-in-one watchlist. Built for HODLERS, traders & cryptocurrency lovers.
>
> Use this cryptocurrency tracker for in-depth crypto chart analysis. Live coin watch 24/7 in the HODL app.

According to the website, it is also possible to sync your portfolio with exchanges or wallets.

This app is **not a wallet** and has no access to your private keys.

