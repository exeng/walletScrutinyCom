---
wsId: metamask
title: MetaMask - Blockchain Wallet
altTitle: 
authors:
- leo
users: 10000000
appId: io.metamask
appCountry: 
released: 2020-09-01
updated: 2023-01-17
version: 5.12.1
stars: 4.5
ratings: 57131
reviews: 3131
size: 
website: https://metamask.io
repository: 
issue: 
icon: io.metamask.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-05-01
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 

---

This is an ETH-only app and thus not a Bitcoin wallet.
