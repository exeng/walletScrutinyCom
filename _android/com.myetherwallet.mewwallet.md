---
wsId: mewEthereum
title: MEW wallet – Ethereum wallet
altTitle: 
authors:
- danny
users: 500000
appId: com.myetherwallet.mewwallet
appCountry: us
released: 2020-03-11
updated: 2022-12-29
version: 2.5.2
stars: 4.5
ratings: 7093
reviews: 547
size: 
website: http://mewwallet.com
repository: 
issue: 
icon: com.myetherwallet.mewwallet.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-02-05
signer: 
reviewArchive: 
twitter: myetherwallet
social:
- https://www.linkedin.com/company/myetherwallet
- https://www.facebook.com/MyEtherWallet
- https://www.reddit.com/r/MyEtherWallet
redirect_from: 

---

Supports 3 chains: Ethereum, Binance and Polygon. 

Does not support BTC.